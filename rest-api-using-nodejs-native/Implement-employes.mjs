import { Employe } from "./Employe.mjs";

export class ImplementEmployes {
  employes = [];
  newEmploye = new Employe();

  initialData() {
    this.newEmploye.setId = 1;
    this.newEmploye.setName = "Maul";
    this.newEmploye.setDivision = "IT";
    this.newEmploye.setPosition = "QA";
    this.employes.push(this.newEmploye);
  }

  getJsonSuccess() {
    return JSON.stringify({
      code: 200,
      message: "Success",
      data: this.employes,
    });
  }

  getJsonFailed(code, message) {
    return JSON.stringify({
      code: code,
      message: message,
    });
  }

  getEmployes(req, resp) {
    resp.write(this.getJsonSuccess());
    resp.end();
  }
  createEmployee(req, resp) {
    req.addListener("data", (data) => {
      let body = JSON.parse(data.toString());
      this.employes.push(body);

      resp.write(this.getJsonSuccess());
      resp.end();
    });
  }

  updateEmployee(req, resp) {
    req.addListener("data", (data) => {
      let body = JSON.parse(data.toString());
      let Index = this.employes.findIndex((employe) => body.id == employe.id);
      if (Index < 0) {
        resp.write(this.getJsonFailed(200, "Employee is not found!"));
        resp.end();
      } else {
        this.employes[Index] = body;
        resp.write(this.getJsonSuccess());
        resp.end();
      }
    });
  }

  deleteEmployee(req, resp) {
    req.addListener("data", (data) => {
      let body = JSON.parse(data.toString());
      let Index = this.employes.findIndex((employe) => body.id == employe.id);
      if (Index < 0) {
        resp.write(this.getJsonFailed(200, "Employee is not found!"));
        resp.end();
      } else {
        this.employes.splice(Index, 1);
        resp.write(this.getJsonSuccess());
        resp.end();
      }
    });
  }
}
