export class Employe {
  constructor(id, name, position, division) {
    (this.id = id), (this.name = name), (this.position = position), (this.division = division);
  }

  // encapsulations
  set setId(id) {
    this.id = id;
  }

  set setName(name) {
    this.name = name;
  }

  set setPosition(position) {
    this.position = position;
  }

  set setDivision(division) {
    this.division = division;
  }

  get getId() {
    return this.id;
  }

  get getName() {
    return this.name;
  }

  get getPosition() {
    return this.position;
  }

  get getDivision() {
    return this.division;
  }
}
