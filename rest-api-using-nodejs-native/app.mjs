import { ImplementEmployes } from "./Implement-employes.mjs";
import http from "http";

const implement = new ImplementEmployes();
implement.initialData();
const server = http.createServer((req, resp) => {
  resp.setHeader("Content-Type", "application/json");

  if (req.method === "GET") {
    implement.getEmployes(req, resp);
  } else if (req.method === "POST") {
    implement.createEmployee(req, resp);
  } else if (req.method === "PUT") {
    implement.updateEmployee(req, resp);
  } else if (req.method === "DELETE") {
    implement.deleteEmployee(req, resp);
  }
});

server.listen(3000);
